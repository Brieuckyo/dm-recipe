<?php

namespace App\Controllers;

use App\Models\Recipe;

class RecipeController
{
    protected $pdo;

    public function __construct(Recipe $pdo)
    {
        $this->pdo = $pdo;
    }

    // GET ALLFROM RECIPE
    public function all()
    {
        $conn = $this->pdo->connect();
        $query = "SELECT * FROM `recipe`";
        $q = $conn->prepare($query);
        $q->execute();
        return json_encode($q->fetchAll());
    }

    // RECIPE
    public function add($name, $persons, $time, $skill)
    {
//        return print_r($name);
        $conn = $this->pdo->connect();
        $query = "INSERT INTO `recipe`(`name`, `persons`, `time`, `skill`) VALUES(:name, :persons, :time, :skill)";
        $q = $conn->prepare($query);
        $q->bindParam(':name', $name);
        $q->bindParam(':persons', $persons);
        $q->bindParam(':time', $time);
        $q->bindParam(':skill', $skill);
        $q->execute();
        $last_id = $conn->lastInsertId();
        return $last_id;
    }

    public function get($id)
    {
        $conn = $this->pdo->connect();
        $query = $query = "SELECT * FROM `recipe` WHERE `id` = :id";
        $q = $conn->prepare($query);
        $q->bindParam(':id', $id);
        $q->execute();
        return json_encode($q->fetchAll());
    }

    public function put($id, $name, $persons, $time, $skill)
    {
        $conn = $this->pdo->connect();
        $query = "UPDATE `recipe` SET `name` = :name, `persons` = :persons, `time` = :time, `skill` = :skill WHERE recipe.id = :id;";
        $q = $conn->prepare($query);
        $q->bindParam(':id', $id);
        $q->bindParam(':name', $name);
        $q->bindParam(':persons', $persons);
        $q->bindParam(':time', $time);
        $q->bindParam(':skill', $skill);
        $q->execute();
        return $id;
    }

    public function delete($id)
    {
        $conn = $this->pdo->connect();
        $query = "
            DELETE `ingredients` FROM `ingredients` INNER JOIN `recipe` ON recipe.id = ingredients.recipeId WHERE recipe.id = :id;
            DELETE `steps` FROM `steps` INNER JOIN `recipe` ON recipe.id = steps.recipeId WHERE recipe.id = :id;
            DELETE FROM `recipe` WHERE `id` = :id;
            ";
        $q = $conn->prepare($query);
        $q->bindParam(':id', $id);
        $q->execute();
        return 'Recipe deleted';
    }

    // INGREDIENTS
    public function addIngredients($recipeId, $description, $quantity)
    {
        $conn = $this->pdo->connect();
        $query = "INSERT INTO `ingredients`(`recipeId`, `description`, `quantity`) VALUES(:recipeId, :description, :quantity)";
        $q = $conn->prepare($query);
        $q->bindParam(':recipeId', $recipeId);
        $q->bindParam(':description', $description);
        $q->bindParam(':quantity', $quantity);
        $q->execute();
        $last_id = $conn->lastInsertId();
        return $last_id;
    }

    public function getIngredients($recipeId)
    {
        $conn = $this->pdo->connect();
        $query = "SELECT ingredients.recipeId, ingredients.description, ingredients.quantity FROM `recipe` INNER JOIN `ingredients` ON ingredients.recipeId = recipe.id WHERE recipe.id = :recipeId";
        $q = $conn->prepare($query);
        $q->bindParam(':recipeId', $recipeId);
        $q->execute();
        return json_encode($q->fetchAll());
    }

    public function deleteIngredients($recipeId)
    {
        $conn = $this->pdo->connect();
        $query = "DELETE `ingredients` FROM `ingredients` INNER JOIN `recipe` ON recipe.id = ingredients.recipeId WHERE recipe.id = :recipeId";
        $q = $conn->prepare($query);
        $q->bindParam(':recipeId', $recipeId);
        $q->execute();
    }

    // STEPS
    public function addSteps($recipeId, $description, $stepNumber)
    {
        $conn = $this->pdo->connect();
        $query = "INSERT INTO `steps`(`recipeId`, `description`, `stepNumber`) VALUES(:recipeId, :description, :stepNumber)";
        $q = $conn->prepare($query);
        $q->bindParam(':recipeId', $recipeId);
        $q->bindParam(':description', $description);
        $q->bindParam(':stepNumber', $stepNumber);
        $q->execute();
        $last_id = $conn->lastInsertId();
        return $last_id;
    }

    public function getSteps($recipeId)
    {
        $conn = $this->pdo->connect();
        $query = "SELECT steps.description, steps.stepNumber FROM `recipe` INNER JOIN `steps` ON steps.recipeId = recipe.id WHERE recipe.id = :recipeId";
        $q = $conn->prepare($query);
        $q->bindParam(':recipeId', $recipeId);
        $q->execute();
        return json_encode($q->fetchAll());
    }

    public function deleteSteps($recipeId)
    {
        $conn = $this->pdo->connect();
        $query = "DELETE `steps` FROM `steps` INNER JOIN `recipe` ON recipe.id = steps.recipeId WHERE recipe.id = :recipeId";
        $q = $conn->prepare($query);
        $q->bindParam(':recipeId', $recipeId);
        $q->execute();
    }

}