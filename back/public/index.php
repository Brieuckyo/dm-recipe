<?php

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age:86400');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token,  Accept, Authorization, X-Requested-With');

require __DIR__ . '/../vendor/autoload.php';

use App\Controllers\RecipeController;
use App\Models\Recipe;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Dotenv\Dotenv;

$dotenv = new Dotenv(__DIR__, '/../.env');
$dotenv->load();

$host = getenv('DATABASE_SERVER');
$dbname = getenv('DATABASE_NAME');
$user = getenv('DATABASE_USER');
$password = getenv('DATABASE_PASSWORD');

$recipe_pdo = new Recipe($host, $dbname, $user, $password);

$recipes = new RecipeController($recipe_pdo);
global $recipes;

$app = new Silex\Application();
$app['debug'] = true;
$app->register(new JDesrosiers\Silex\Provider\CorsServiceProvider(), [
    "cors.allowOrigin" => "http://localhost:8080/",
]);


// GET ALL FROM RECIPE
$app->get('/all', function () use ($app) {
    global $recipes;
    return $recipes->all();
});


// RECIPE
$app->post('/add', function (Request $request) use ($app) {
    global $recipes;
    $json = json_decode(file_get_contents("php://input"), true);
    return $recipes->add($json['name'], $json['persons'], $json['time'], $json['skill']);
});

$app->get('/recipe/{id}', function($id) use ($app   ) {
    global $recipes;
    return $recipes->get($id);
});

$app->put('/recipe/{id}/update', function($id) use ($app) {
    global $recipes;
    $json = json_decode(file_get_contents("php://input"), true);
    return $recipes->put($id, $json['name'], $json['persons'], $json['time'], $json['skill']);
});

$app->delete('/recipe/{id}/delete', function($id) use ($app) {
    global $recipes;
    return $recipes->delete($id);
});


// INGREDIENTS
$app->post('/add/ingredients', function (Request $request) use ($app) {
    global $recipes;
    $json = json_decode(file_get_contents("php://input"), true);
    foreach ($json['ingredients'] as $key => $ingredient) {
        $result = $recipes->addIngredients($json['recipeId'], $ingredient['description'], $ingredient['quantity']);
    }
    return $result;
});

$app->get('/recipe/{id}/ingredients', function($id) use ($app) {
    global $recipes;
    return $recipes->getIngredients($id);
});

$app->put('/recipe/{id}/ingredients', function($id) use ($app) {
    global $recipes;
    $json = json_decode(file_get_contents("php://input"), true);
    $recipes->deleteIngredients($id);
    foreach ($json['ingredients'] as $key => $ingredient) {
        $result = $recipes->addIngredients($id, $ingredient['description'], $ingredient['quantity']);
    }
    return $result;
});


// STEPS
$app->post('/add/steps', function (Request $request) use ($app) {
    global $recipes;
    $json = json_decode(file_get_contents("php://input"), true);
    foreach ($json['steps'] as $key => $step) {
        $result = $recipes->addSteps($json['recipeId'], $step['description'], $step['stepNumber']);
    }
    return $result;
});

$app->get('/recipe/{id}/steps', function($id) use ($app) {
    global $recipes;
    return $recipes->getSteps($id);
});

$app->put('/recipe/{id}/steps', function($id) use ($app) {
    global $recipes;
    $json = json_decode(file_get_contents("php://input"), true);
    $recipes->deleteSteps($id);
    foreach ($json['steps'] as $key => $step) {
        $result = $recipes->addSteps($id, $step['description'], $step['stepNumber']);
    }
    return $result;
});

$app["cors-enabled"]($app);
$app->run();

?>