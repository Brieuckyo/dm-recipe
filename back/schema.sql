DROP DATABASE IF EXISTS `recipes`;
CREATE DATABASE `recipes`;
USE `recipes`;
CREATE TABLE `recipe` (
	`id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(250) NOT NULL,
	`persons` varchar(10) NOT NULL,
	`time` varchar(250) NOT NULL,
	`skill` varchar(250) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `ingredients` (
	`id` int NOT NULL AUTO_INCREMENT,
	`recipeId` int NOT NULL,
	`description` varchar(250) NOT NULL,
	`quantity` int NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `steps` (
	`id` int NOT NULL AUTO_INCREMENT,
	`recipeId` int NOT NULL,
	`description` varchar(250) NOT NULL,
	`stepNumber` int NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `ingredients` ADD CONSTRAINT `ingredients_fk0` FOREIGN KEY (`recipeId`) REFERENCES `recipe`(`id`);
ALTER TABLE `steps` ADD CONSTRAINT `steps_fk0` FOREIGN KEY (`recipeId`) REFERENCES `recipe`(`id`);

INSERT into `recipe` (`name`, `persons`, `time`, `skill`) VALUES ("Brochettes de poulet au barbecue", 2, "01h05", "Très facile");

INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (1, "blancs de poulet", "2");
INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (1, "oignon rouge", "1");
INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (1, "cuillères à soupe de jus de citron", "2");
INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (1, "gousse d'ail", "1");
INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (1, "cuillère à soupe d'huile d'olive", "1");

INSERT into `steps` (`recipeId`, `description`, `stepNumber`) VALUES (1, "Epluchez et émincez grossièrement l'oignon rouge. Coupez les blancs de poulet en morceaux.", 1);
INSERT into `steps` (`recipeId`, `description`, `stepNumber`) VALUES (1, "Epluchez et pressez l'ail. Dans un bol, mélangez l'huile d'olive, le jus de citron et l'ail. Salez, poivrez et laissez mariner les blancs de poulet dans ce mélange pendant 40 minutes.", 2);

INSERT into `recipe` (`name`, `persons`, `time`, `skill`) VALUES ("Cake aux pépites de chocolat", 6, "00h45", "Facile");

INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (2, "Sachet de farine", "1");
INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (2, "Sachet de sucre en poudre", "1");
INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (2, "Sachet de levure chimique", "2");
INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (2, "Oeuf", "1");
INSERT into `ingredients` (`recipeId`, `description`, `quantity`) VALUES (2, "Pépites de chocolats", "7");

INSERT into `steps` (`recipeId`, `description`, `stepNumber`) VALUES (2, "Préchauffez votre four à 180°.", 1);
INSERT into `steps` (`recipeId`, `description`, `stepNumber`) VALUES (2, "Dans une jatte, versez les oeufs, le sucre, le sucre vanillé, le yaourt nature et fouettez le tout.", 2);
INSERT into `steps` (`recipeId`, `description`, `stepNumber`) VALUES (2, "Versez petit à petit la farine tout en continuant de mélanger puis ajoutez la levure, les pépites de chocolat et l'huile..", 3);
INSERT into `steps` (`recipeId`, `description`, `stepNumber`) VALUES (2, "Versez la préparation bien mélangée dans un moule à cake préalablement beurré et fariné.", 4);
INSERT into `steps` (`recipeId`, `description`, `stepNumber`) VALUES (2, "Enfournez environ 35 minutes à 180°.", 5);
