# dm-recipe

> Recipe project with VueJs & PHP

## Environment used

- Works fine with node v11.5.0
- Works fine with php v7.1.25

## /front

``` bash

# go to directory /front
cd /front

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

## /back

``` bash

# go to directory /back
cd /back

# start server on port 8887
php -S localhost:8887

# import database schema
/back/schema.sql
```
