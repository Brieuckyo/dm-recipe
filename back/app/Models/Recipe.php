<?php

namespace App\Models;

use PDO;
use PDOException;

class Recipe
{
    protected $host;
    protected $dbname;
    protected $user;
    protected $password;
    protected $pdo;

    /**
     * PDOController constructor.
     * @param $host
     * @param $dbname
     * @param $user
     * @param $password
     */
    public function __construct($host, $dbname, $user, $password)
    {
        $this->host = $host;
        $this->dbname = $dbname;
        $this->user = $user;
        $this->password = $password;
    }

    public function connect()
    {
        $this->pdo = null;

        try {
            $this->pdo = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("Connection error {$e->getMessage()}");
        }

        return $this->pdo;
    }
}